package com.o4oxide.peoplecolony.core.domain

import com.o4oxide.peoplecolony.core.domain.rules.RuleViolationException
import com.o4oxide.peoplecolony.core.domain.rules.validation.EmailValidationRule
import spock.lang.Specification

class EmailValidationRuleTest extends Specification {

    EmailValidationRule rule

    def setup() {
        rule = new EmailValidationRule()
    }

    def "email validation works"() {
        when:
        rule.validate(email)

        then:
        check()

        where:
        email         || check
        "abc@def.com" || { notThrown(RuleViolationException) }
        "abadef"      || { thrown(RuleViolationException) }
    }
}
