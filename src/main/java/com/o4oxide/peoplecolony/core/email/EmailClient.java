package com.o4oxide.peoplecolony.core.email;

public interface EmailClient {

    void send(Email email);
}
