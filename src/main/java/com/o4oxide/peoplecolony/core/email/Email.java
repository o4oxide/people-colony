package com.o4oxide.peoplecolony.core.email;


import com.o4oxide.peoplecolony.core.domain.common.EmailAddress;

public class Email {

    private final EmailAddress from;
    private final EmailAddress to;
    private final String body;


    public Email(EmailAddress from, EmailAddress to, String body) {
        this.from = from;
        this.to = to;
        this.body = body;
    }

    public EmailAddress getFrom() {
        return from;
    }

    public EmailAddress getTo() {
        return to;
    }

    public String getBody() {
        return body;
    }
}
