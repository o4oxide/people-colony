package com.o4oxide.peoplecolony.core.domain.rules.validation;

import com.o4oxide.peoplecolony.core.domain.rules.RuleViolationException;

import java.util.regex.Pattern;

public class PasswordValidationRule extends ValidationRule {

    private static final Pattern PATTERN = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_-])[a-zA-z0-9!@#$%^&*_-]{8,}$");
    private static final String PATTERN_VIOLATION_MESSAGE = "Password must be min of 8 characters containing at least one of a-z, A-Z, special symbols: !@#$%^&*_-";
    private static final int MAX_SIZE = 200;

    private final int size;
    public PasswordValidationRule(int size) {
        this.size = size;
    }

    public PasswordValidationRule() {
        this(MAX_SIZE);
    }

    public void validate(String password) {
        if (password == null) {
            throw new RuleViolationException("password is empty");
        }
        if (password.length() > size) {
            throw new RuleViolationException("password is too long");
        }
        if (!PATTERN.matcher(password).matches()) {
            throw new RuleViolationException(PATTERN_VIOLATION_MESSAGE);
        }
    }

    @Override
    public String getViolationMessage() {
        return PATTERN_VIOLATION_MESSAGE;
    }
}
