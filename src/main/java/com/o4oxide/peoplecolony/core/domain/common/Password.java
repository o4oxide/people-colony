package com.o4oxide.peoplecolony.core.domain.common;

import com.o4oxide.peoplecolony.core.domain.rules.validation.PasswordValidationRule;
import com.o4oxide.peoplecolony.utils.Security;
import java.nio.charset.StandardCharsets;

public class Password {

    private static final PasswordValidationRule passwordRule = new PasswordValidationRule();

    private final String digest;

    public Password(String rawPassword) {
        passwordRule.validate(rawPassword);
        this.digest = Security.digest(rawPassword, StandardCharsets.UTF_8);
    }

    public String getDigest() {
        return this.digest;
    }
}
