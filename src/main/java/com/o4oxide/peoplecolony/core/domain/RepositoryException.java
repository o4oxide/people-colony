package com.o4oxide.peoplecolony.core.domain;

public class RepositoryException extends RuntimeException {

    public RepositoryException(Throwable ex) {
        super(ex);
    }
}
