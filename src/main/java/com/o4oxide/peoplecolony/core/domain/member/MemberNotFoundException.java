package com.o4oxide.peoplecolony.core.domain.member;

public class MemberNotFoundException extends RuntimeException {

    public MemberNotFoundException() {
        super();
    }
}
