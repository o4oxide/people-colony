package com.o4oxide.peoplecolony.core.domain;

import java.time.OffsetDateTime;

public abstract class Entity<T extends Entity<?>> {

    protected OffsetDateTime createdDate;
    protected OffsetDateTime updatedDate;

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public T setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
        return (T) this;
    }

    public OffsetDateTime getUpdatedDate() {
        return updatedDate;
    }

    public T setUpdatedDate(OffsetDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return (T) this;
    }
}
