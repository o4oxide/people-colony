package com.o4oxide.peoplecolony.core.domain.rules.validation;

import com.o4oxide.peoplecolony.core.domain.rules.RuleViolationException;
import com.o4oxide.peoplecolony.utils.StringUtils;

import java.util.regex.Pattern;

public class EmailValidationRule extends ValidationRule {

    private static final Pattern PATTERN = Pattern.compile("^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$");
    private static final String PATTERN_VIOLATION_MESSAGE = "Invalid email. Ensure email contains @";
    private static final int DEFAULT_SIZE = 50;

    private final int size;

    public EmailValidationRule(int size) {
        this.size = size;
    }

    public EmailValidationRule() {
        this(DEFAULT_SIZE);
    }

    public void validate(String email) {
        if (StringUtils.isBlank(email)) {
            throw new RuleViolationException("email is empty");
        }
        if (email.length() > size) {
            throw new RuntimeException("email is too long");
        }
        if (!PATTERN.matcher(email).matches()) {
            throw new RuleViolationException(PATTERN_VIOLATION_MESSAGE);
        }
    }

    @Override
    public String getViolationMessage() {
        return PATTERN_VIOLATION_MESSAGE;
    }
}
