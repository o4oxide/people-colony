package com.o4oxide.peoplecolony.core.domain.member;

public class UniqueMemberException extends RuntimeException {

    public UniqueMemberException(String message) {
        super(message);
    }
}
