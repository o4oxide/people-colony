package com.o4oxide.peoplecolony.core.domain.common;

import com.o4oxide.peoplecolony.core.domain.rules.validation.EmailValidationRule;

import java.util.Objects;

public class EmailAddress {

    private static final EmailValidationRule emailRule = new EmailValidationRule();

    private final String value;

    public EmailAddress(String value) {
        emailRule.validate(value);
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailAddress that = (EmailAddress) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
