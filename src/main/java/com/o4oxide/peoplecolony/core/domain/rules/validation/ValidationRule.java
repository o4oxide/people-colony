package com.o4oxide.peoplecolony.core.domain.rules.validation;

public abstract class ValidationRule {

    public abstract String getViolationMessage();
}
