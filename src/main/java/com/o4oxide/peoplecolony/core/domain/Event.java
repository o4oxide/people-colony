package com.o4oxide.peoplecolony.core.domain;

import java.time.OffsetDateTime;

public class Event {

    protected final long eventId;
    protected final EventType eventType;
    protected final String username;
    protected final byte[] payload;
    protected OffsetDateTime createdDate;

    public Event(long eventId, EventType eventType, String username, byte[] payload) {
        this.eventId = eventId;
        this.eventType = eventType;
        this.username = username;
        this.payload = payload;
    }

    public long getEventId() {
        return eventId;
    }

    public byte[] getPayload() {
        return payload;
    }

    public EventType getEventType() {
        return eventType;
    }

    public String getUsername() {
        return username;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public Event setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }
}
