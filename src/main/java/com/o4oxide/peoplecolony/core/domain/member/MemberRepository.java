package com.o4oxide.peoplecolony.core.domain.member;

import com.o4oxide.peoplecolony.core.domain.common.EmailAddress;


public interface MemberRepository {
    void saveMember(Member member);
    Member retrieveMember(EmailAddress email);
}
