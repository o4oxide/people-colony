package com.o4oxide.peoplecolony.core.domain.member;

import com.o4oxide.peoplecolony.core.domain.Entity;
import com.o4oxide.peoplecolony.core.domain.common.EmailAddress;
import com.o4oxide.peoplecolony.core.domain.common.Password;


public class Member extends Entity<Member> {

    private final long memberId;
    private final EmailAddress email;
    private final State state;
    private final Password password;

    public enum State {
        NEW,
        CONFIRMED,
        DELETED,
    }

    public Member(long memberId, EmailAddress email, State state) {
        this.memberId = memberId;
        this.email = email;
        this.password = null;
        this.state = state;
    }

    public Member(long memberId, EmailAddress email, Password password, State state) {
        this.memberId = memberId;
        this.email = email;
        this.password = password;
        this.state = state;
    }

    public long getMemberId() {
        return memberId;
    }

    public EmailAddress getEmail() {
        return email;
    }

    public State getState() {
        return state;
    }

    public Password getPassword() {
        return password;
    }
}
