package com.o4oxide.peoplecolony.core.domain.rules;

public class RuleViolationException extends RuntimeException {

    public RuleViolationException(String message) {
        super(message);
    }
}
