package com.o4oxide.peoplecolony.core.application.signup;

import static com.o4oxide.peoplecolony.core.domain.member.Member.State;

import com.o4oxide.peoplecolony.core.domain.member.Member;
import com.o4oxide.peoplecolony.core.domain.member.MemberRepository;
import com.o4oxide.peoplecolony.utils.Snowflake;
import javax.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class SignupService {

  private final MemberRepository memberRepository;

  public SignupService(MemberRepository memberRepository) {
    this.memberRepository = memberRepository;
  }

  public SignupResponse signup(SignupRequest signupRequest) {
    long memberId = Snowflake.getNextId();
    Member member = new Member(memberId
                              ,signupRequest.getEmail()
                              ,signupRequest.getPassword()
                              ,State.NEW);
    memberRepository.saveMember(member);
    return new SignupResponse(member.getMemberId(), member.getEmail(), member.getState());
  }

  public SignupResponse signupStatus(SignupRequest signupRequest) {
    Member member = memberRepository.retrieveMember(signupRequest.getEmail());
    return new SignupResponse(member.getMemberId(), member.getEmail(), member.getState());
  }
}
