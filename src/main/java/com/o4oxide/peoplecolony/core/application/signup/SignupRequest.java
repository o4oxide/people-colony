package com.o4oxide.peoplecolony.core.application.signup;

import com.o4oxide.peoplecolony.core.domain.common.EmailAddress;
import com.o4oxide.peoplecolony.core.domain.common.Password;

public class SignupRequest {

    private final EmailAddress email;
    private final Password password;

    public SignupRequest(EmailAddress email, Password password) {
        this.email = email;
        this.password = password;
    }

    public SignupRequest(EmailAddress email) {
        this.email = email;
        this.password = null;
    }

    public EmailAddress getEmail() {
        return email;
    }

    public Password getPassword() {
        return password;
    }
}
