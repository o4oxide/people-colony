package com.o4oxide.peoplecolony.core.application.signup;

import com.o4oxide.peoplecolony.core.domain.common.EmailAddress;
import com.o4oxide.peoplecolony.core.domain.member.Member;

public class SignupResponse {

    private final long memberId;
    private final EmailAddress email;
    private final Member.State state;

    public SignupResponse(long memberId, EmailAddress email, Member.State state) {
        this.memberId = memberId;
        this.email = email;
        this.state = state;
    }

    public long getMemberId() {
        return memberId;
    }

    public EmailAddress getEmail() {
        return email;
    }

    public Member.State getState() {
        return state;
    }
}
