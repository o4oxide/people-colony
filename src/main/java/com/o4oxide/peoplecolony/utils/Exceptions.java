package com.o4oxide.peoplecolony.utils;

import java.util.concurrent.CompletionException;
import java.util.function.Consumer;

public final class Exceptions {

    private Exceptions() {
    }

    public static Throwable unwrap(Throwable ex) {
        if (ex == null || ex.getCause() == null) {
            return ex;
        }
        if (ex instanceof CompletionException) {
            return ex.getCause();
        }
        return ex;
    }
}
