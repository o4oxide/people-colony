package com.o4oxide.peoplecolony.utils;

import java.util.List;

public final class ListUtils {

    private ListUtils() {}

    public static <T> T getLast(List<T> list) {
        RequireUtil.require(CollectionUtils::isNotEmpty, list, "list");
        return list.get(list.size() - 1);
    }
}
