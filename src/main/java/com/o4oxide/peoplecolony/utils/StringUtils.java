package com.o4oxide.peoplecolony.utils;

public final class StringUtils {

    private StringUtils(){}

    public static boolean isBlank(String value) {
        return value == null || value.isBlank();
    }

    public static boolean isNotBlank(String value) {
        return !isBlank(value);
    }

    public static boolean isSurroundedBy(String value, String wrapper) {
        if (isBlank(value) || isBlank(wrapper)) {
            return false;
        }
        return value.startsWith(wrapper) && value.endsWith(wrapper);
    }

    public static String stripWrapper(String value, String wrapper) {
        if (isBlank(value) || isBlank(wrapper)) {
            return value;
        }
        if (value.startsWith(wrapper) && value.endsWith(wrapper)) {
            int beginIndex = wrapper.length();
            int endIndex = value.length() - wrapper.length();
            return value.substring(beginIndex, endIndex);
        }
        return value;
    }

    public static boolean containsAny(String value, String... values) {
        if (isBlank(value) || values == null || values.length == 0) {
            return false;
        }
        for (String val : values) {
            if (val.equals(value)) {
                return true;
            }
        }
        return false;
    }

    static <T> String join(String separator, T... values) {
        StringBuilder stringBuilder = new StringBuilder();
        String sep = separator != null ? separator : "";
        for (int i = 0; i < values.length; i++) {
            stringBuilder.append(values[i].toString());
            if (i < (values.length - 1)) {
                stringBuilder.append(sep);
            }
        }
        return stringBuilder.toString();
    }

}
