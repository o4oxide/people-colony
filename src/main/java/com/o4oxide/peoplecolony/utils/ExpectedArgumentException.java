package com.o4oxide.peoplecolony.utils;

public class ExpectedArgumentException extends RuntimeException {

    public ExpectedArgumentException(String message) {
        super(message);
    }
}
