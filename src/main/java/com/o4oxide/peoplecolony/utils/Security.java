package com.o4oxide.peoplecolony.utils;

import io.quarkus.dev.appstate.ApplicationStartException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Security {

    private Security(){}

    private static final MessageDigest digester;

    static {
        try {
            digester = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new ApplicationStartException(e);
        }
    }

    public static String digest(String data, Charset charset) {
       return new String(digester.digest(data.getBytes(charset)), charset);
    }
}
