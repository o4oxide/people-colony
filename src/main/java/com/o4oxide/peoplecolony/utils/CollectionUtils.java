package com.o4oxide.peoplecolony.utils;


import io.vertx.core.json.JsonArray;

import java.util.Collection;

import static com.o4oxide.peoplecolony.utils.StringUtils.join;

public final class CollectionUtils {

    private CollectionUtils(){}

    public static <T> boolean isEmpty(Collection<T> collection) {
        return collection == null || collection.size() == 0;
    }

    public static <T> boolean isNotEmpty(Collection<T> collection) {
        return !isEmpty(collection);
    }

    public static <T> boolean contains(Collection<T> collection, T value) {
        if (value == null || collection == null || collection.size() == 0) {
            return false;
        }
        for (T item : collection) {
            if (value.equals(item)) {
                return true;
            }
        }
        return false;
    }

    public static <T> boolean contains(JsonArray jsonArray, T value) {
        if (value == null || jsonArray == null || jsonArray.size() == 0) {
            return false;
        }
        return jsonArray.contains(value);
    }

    public static <T> boolean notIn(Collection<T> collection, T value) {
       return !contains(collection, value);
    }

    public static <T> String toString(Collection<T> collection) {
        if (collection == null) {
            return "null";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        join(",", collection.toArray());
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
