package com.o4oxide.peoplecolony.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;

public final class Time {
    private Time() {}

    public static final ZoneId UTC_ZONE =ZoneId.of("UTC");

    public static LocalDateTime now() {
        return LocalDateTime.now(UTC_ZONE);
    }
}
