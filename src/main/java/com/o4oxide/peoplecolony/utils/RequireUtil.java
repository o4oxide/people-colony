package com.o4oxide.peoplecolony.utils;


import java.util.function.Predicate;

public final class RequireUtil {

    private RequireUtil() {}

    public static void require(String value, String title) {
        if (StringUtils.isBlank(value)) {
            throw new ExpectedArgumentException(title + " cannot be blank");
        }
    }

    public static void require(Object value, String title) {
        if (value == null) {
            throw new ExpectedArgumentException(title + " cannot be null");
        }
    }

    public static <T> void require(Predicate<T> predicate, T value, String title) {
        if (!predicate.test(value)) {
            throw new ExpectedArgumentException(title + " failed predicate test");
        }
    }

    public static void requireOneOf(String[] expected, String value, String title) {
        if (ArrayUtils.notIn(expected, value)) {
            throw new ExpectedArgumentException(title + " must be one of " + ArrayUtils.toString(expected));
        }
    }
}
