package com.o4oxide.peoplecolony.utils;

import static com.o4oxide.peoplecolony.utils.StringUtils.join;

public final class ArrayUtils {

    private ArrayUtils(){}

    public static <T> boolean isEmpty(T[] array) {
        return array == null || array.length == 0;
    }

    public static <T> boolean isNotEmpty(T[] array) {
        return !isEmpty(array);
    }

    public static <T> boolean contains(T[] array, T value) {
        if (value == null || array == null || array.length == 0) {
            return false;
        }
        for (T item : array) {
            if (value.equals(item)) {
                return true;
            }
        }
        return false;
    }

    public static <T> boolean notIn(T[] array, T value) {
       return !contains(array, value);
    }

    public static <T> String toString(T[] array) {
        if (array == null) {
            return "null";
        }
        return "[" + join(",", array) + "]";
    }
}
