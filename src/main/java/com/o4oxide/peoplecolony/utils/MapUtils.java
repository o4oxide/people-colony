package com.o4oxide.peoplecolony.utils;

import java.util.Map;

public final class MapUtils {

    private MapUtils() {}

    public static <K,V> boolean isEmpty(Map<K, V> map) {
        return map == null || map.isEmpty();
    }

    public static <K,V> boolean isNotEmpty(Map<K, V> map) {
        return !isEmpty(map);
    }
}
