package com.o4oxide.peoplecolony.data.sql;

public class DataSourceException extends RuntimeException {

    public DataSourceException(String message) {
      super(message);
    }
}
