package com.o4oxide.peoplecolony.data.sql;

import io.agroal.api.AgroalDataSource;
import io.quarkus.runtime.configuration.ConfigurationException;
import javax.validation.constraints.NotNull;
import org.jdbi.v3.core.Jdbi;


public abstract class AbstractSQLDataSource {

  protected final Jdbi jdbi;

  public AbstractSQLDataSource(AgroalDataSource dataSource) {
    if (dataSource == null) {
      throw new ConfigurationException("dataSource was not configured");
    }
    jdbi = Jdbi.create(dataSource);
  }

}
