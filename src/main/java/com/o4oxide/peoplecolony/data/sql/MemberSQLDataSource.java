package com.o4oxide.peoplecolony.data.sql;

import com.o4oxide.peoplecolony.core.domain.common.EmailAddress;
import com.o4oxide.peoplecolony.core.domain.member.Member;
import com.o4oxide.peoplecolony.core.domain.member.MemberRepository;
import io.agroal.api.AgroalDataSource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class MemberSQLDataSource extends AbstractSQLDataSource implements MemberRepository {

    private static final String INSERT_MEMBER =
        "INSERT INTO members (member_id, email, password, state) " +
        "VALUES(:memberId, :email, :password, :state);";
    private static final String SELECT_MEMBER_BY_EMAIL =
        "SELECT * FROM members WHERE email = $1 AND state != 'DELETED';";

    public MemberSQLDataSource() {
        this(null);
    }

    @Inject
    public MemberSQLDataSource(AgroalDataSource dataSource) {
        super(dataSource);
    }

    @Override
    public void saveMember(Member member) {
        jdbi.useHandle(handle ->
            handle.createUpdate(INSERT_MEMBER)
                  .bindBean(member));
    }

    @Override
    public Member retrieveMember(EmailAddress email) {
        return (Member)
            jdbi.withHandle(handle ->
                handle.createQuery(SELECT_MEMBER_BY_EMAIL)
                      .bind("email", email.toString())
                      .mapTo(Member.class)
              );
    }
}
