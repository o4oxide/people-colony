package com.o4oxide.peoplecolony.interfaces.api;

import com.o4oxide.peoplecolony.core.domain.member.MemberNotFoundException;
import com.o4oxide.peoplecolony.core.domain.member.UniqueMemberException;
import com.o4oxide.peoplecolony.core.domain.rules.RuleViolationException;
import com.o4oxide.peoplecolony.utils.Exceptions;
import io.vertx.core.json.JsonObject;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Resource {

    private final Logger log = LoggerFactory.getLogger(getClass());

    protected Response handleException(Throwable ex) {
        Throwable error = Exceptions.unwrap(ex);
        log.error("Error in resource", error);
        JsonObject result = new JsonObject()
                .put("error", true)
                .put("message", getErrorMessage(error));
        return Response.status(getErrorCode(error))
                        .entity(result)
                        .build();
    }


    protected String getErrorMessage(Throwable ex) {
        if (ex instanceof UniqueMemberException) {
            return ex.getMessage();
        } else if (ex instanceof RuleViolationException) {
            return ex.getMessage();
        } else {
            return "An error occured in the backend. We will find and resolve it.";
        }
    }

    protected int getErrorCode(Throwable ex) {
        if (ex instanceof UniqueMemberException) {
            return 409;
        } else if (ex instanceof RuleViolationException) {
            return 400;
        } else if (ex instanceof MemberNotFoundException) {
            return 404;
        } else {
            return 500;
        }
    }
}
