package com.o4oxide.peoplecolony.interfaces.api;

import com.o4oxide.peoplecolony.core.application.signup.SignupRequest;
import com.o4oxide.peoplecolony.core.application.signup.SignupResponse;
import com.o4oxide.peoplecolony.core.application.signup.SignupService;
import com.o4oxide.peoplecolony.core.domain.common.EmailAddress;
import com.o4oxide.peoplecolony.core.domain.rules.RuleViolationException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("api/signup")
public class SignupResource extends Resource {

    @Inject
    SignupService signupService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signup(SignupRequest request) {
        try {
            SignupResponse response = signupService.signup(request);
            return Response.status(Status.CREATED)
                            .entity(response)
                            .build();

        } catch (RuleViolationException ex) {
            return handleException(ex);
        }
    }

    @Path("{email}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response status(@PathParam("email") String email) {
        try {
            EmailAddress emailAddress = new EmailAddress(email);
            SignupRequest signupRequest = new SignupRequest(emailAddress, null);
            SignupResponse response = signupService.signupStatus(signupRequest);
            return Response.ok(response).build();
        } catch (RuleViolationException ex) {
            return handleException(ex);
        }
    }
}
