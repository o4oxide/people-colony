#! /bin/bash

dbname=""
appuser=""

for arg in "$@"
do
	key=$(echo $arg | tr -d " " | cut -f1 -d=)
	val=$(echo $arg | tr -d " " | cut -f2 -d=)
	case $key in
		database) dbname=$val;;
		 appuser) appuser=$val;;
		       *) echo "Unknown parameter $key ... will continue without processing this parameter";;
	esac
done

if [[ -z $dbname ]]
then
	echo "No database parameter was specified. Please pass it in as database=<database>name"
	exit 1
fi

if [[ -z $appuser ]]
then
	echo "No appuser parameter was specified. Please pass it in as appuser=<appuser>value"
	exit 1
fi

read -sp "Enter Flyway database password: " dbpass

FLHOME="/usr/share/flyway7"
FLHOST="postgres.o4oxide.com"

url="jdbc:postgresql://$FLHOST:5432/$dbname?sslmode=verify-full&sslcert=$FLHOME/certs/flyway.o4oxide.com.crt&sslkey=$FLHOME/keys/flyway.o4oxide.com.key.pk8&sslrootcert=$FLHOME/certs/o4oxide.com.crt"

FLYWAY_URL=$url FLYWAY_USER=flyway FLYWAY_PASSWORD=$dbpass FLYWAY_PLACEHOLDERS_APPUSER=$appuser FLYWAY_LOCATIONS=filesystem:. /usr/share/flyway7/flyway migrate -X


