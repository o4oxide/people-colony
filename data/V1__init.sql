SELECT 'init' FROM pg_create_logical_replication_slot(CONCAT(current_database(), '_slot'), 'wal2json');

GRANT USAGE ON SCHEMA ${flyway:defaultSchema} TO ${appuser};

CREATE TABLE members (
    member_id BIGINT PRIMARY KEY NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(512) NOT NULL,
    state VARCHAR(50) NOT NULL,
    created_date TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_date TIMESTAMPTZ
);

GRANT SELECT, INSERT, UPDATE ON members TO ${appuser};

ALTER TABLE members REPLICA IDENTITY FULL;

CREATE SCHEMA cdc;

GRANT ALL PRIVILEGES ON SCHEMA cdc to ${appuser};

ALTER TABLE members REPLICA IDENTITY FULL;


