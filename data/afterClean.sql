SELECT 'stop'
FROM pg_drop_replication_slot(current_database() || '_slot')
WHERE (SELECT slot_name FROM pg_replication_slots WHERE slot_name = current_database() || '_slot') IS NOT NULL;

DROP SCHEMA IF EXISTS cdc;

DROP TABLE IF EXISTS waltest;